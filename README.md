git-autofix-proofs
==================

Proof of concepts for git autofix

[jlroberts@ip-10-202-193-192 autofix]$ ls
git-autofix  README.md
[jlroberts@ip-10-202-193-192 autofix]$ cd ..
[jlroberts@ip-10-202-193-192 bin]$ mkdir temp_orig
[jlroberts@ip-10-202-193-192 bin]$ mkdir temp_new
[jlroberts@ip-10-202-193-192 bin]$ nano -w temp_new/test.txt
[jlroberts@ip-10-202-193-192 bin]$ mv temp_new/test.txt .
[jlroberts@ip-10-202-193-192 bin]$ cd temp_new
[jlroberts@ip-10-202-193-192 temp_new]$ git clone git@github.com:jroberts0001/git-autofix-proofs.git .
Cloning into ....
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
[jlroberts@ip-10-202-193-192 temp_new]$ cd ..
[jlroberts@ip-10-202-193-192 bin]$ mv test.txt temp_new
[jlroberts@ip-10-202-193-192 bin]$ cd temp_new
[jlroberts@ip-10-202-193-192 temp_new]$ git add .
[jlroberts@ip-10-202-193-192 temp_new]$ git commit -m 'initial commit'
[master f73f843] initial commit
 1 files changed, 7 insertions(+), 0 deletions(-)
 create mode 100644 test.txt
[jlroberts@ip-10-202-193-192 temp_new]$ git push
Counting objects: 4, done.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 320 bytes, done.
Total 3 (delta 0), reused 0 (delta 0)
To git@github.com:jroberts0001/git-autofix-proofs.git
   1c08763..f73f843  master -> master
[jlroberts@ip-10-202-193-192 temp_new]$ cd ..
[jlroberts@ip-10-202-193-192 bin]$ cd temp_orig
[jlroberts@ip-10-202-193-192 temp_orig]$ git clone git@github.com:jroberts0001/git-autofix-proofs.git .
Cloning into ....
remote: Counting objects: 6, done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 6 (delta 0), reused 3 (delta 0)
Receiving objects: 100% (6/6), done.
[jlroberts@ip-10-202-193-192 temp_orig]$ nano test.txt
[jlroberts@ip-10-202-193-192 temp_orig]$ git commit -am 'altered the test text file from original'
[master 70ad678] altered the test text file from original
 1 files changed, 2 insertions(+), 1 deletions(-)
[jlroberts@ip-10-202-193-192 temp_orig]$ git push
Counting objects: 5, done.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 351 bytes, done.
Total 3 (delta 0), reused 0 (delta 0)
To git@github.com:jroberts0001/git-autofix-proofs.git
   f73f843..70ad678  master -> master
[jlroberts@ip-10-202-193-192 bin]$ cd temp_new
[jlroberts@ip-10-202-193-192 temp_new]$ git commit -am 'creating conflict'
[master ac477b8] creating conflict
 1 files changed, 1 insertions(+), 0 deletions(-)
[jlroberts@ip-10-202-193-192 temp_new]$ git push
To git@github.com:jroberts0001/git-autofix-proofs.git
 ! [rejected]        master -> master (non-fast-forward)
error: failed to push some refs to 'git@github.com:jroberts0001/git-autofix-proofs.git'
To prevent you from losing history, non-fast-forward updates were rejected
Merge the remote changes (e.g. 'git pull') before pushing again.  See the
'Note about fast-forwards' section of 'git push --help' for details.
[jlroberts@ip-10-202-193-192 temp_new]$ git pull
remote: Counting objects: 5, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 3 (delta 0)
Unpacking objects: 100% (3/3), done.
From github.com:jroberts0001/git-autofix-proofs
   f73f843..70ad678  master     -> origin/master
Auto-merging test.txt
Merge made by recursive.
 test.txt |    3 ++-
 1 files changed, 2 insertions(+), 1 deletions(-)
